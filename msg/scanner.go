package msg

import (
	"fmt"
	"strings"
	"unicode"
)

type Message interface {
	fmt.Scanner
	fmt.Formatter
}

type IPubSub interface {
	Message
}

type PubSub struct {
	Topic    string
	Messages []string
}

//Scan topics and Message is scanned to the struct
func (pm *PubSub) Scan(state fmt.ScanState, _ rune) error {
	token, err := state.Token(false, func(r rune) bool {
		return r != '@'
	})
	if err != nil {
		return err
	}
	_, _, _ = state.ReadRune()
	pm.Topic = string(token)
	token, err = state.Token(false, nil)
	if err != nil {
		return err
	}
	pm.Messages = strings.Split(string(token), ":")
	return nil
}

func (pm *PubSub) Format(f fmt.State, _ rune) {
	t := pm.Topic
	m := strings.Join(pm.Messages, ":")

	_, err := f.Write([]byte(t + "@" + m))
	if err != nil {
		return
	}
}

type IPushPull interface {
	Message
}

type PushPull struct {
	Msg string
}

func (p *PushPull) Scan(state fmt.ScanState, verb rune) (err error) {
	token, err := state.Token(true, unicode.IsLetter)
	p.Msg = string(token)
	return
}

func (p *PushPull) Format(f fmt.State, c rune) {
	_, _ = f.Write([]byte(p.Msg))
}
