module github.com/shipp02/go-rov

go 1.14

require (
	github.com/zeromq/goczmq v4.1.0+incompatible
	gobot.io/x/gobot v1.14.0
)
