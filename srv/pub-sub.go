package srv

import (
	"fmt"
	"github.com/shipp02/go-rov/msg"
	zmq "github.com/zeromq/goczmq"
	"log"
	"strconv"
	"strings"
)

type sender interface {
	SendMessage([][]byte) error
	SendFrame(data []byte, flags int) error
}

type reciever interface {
	RecvFrame() ([]byte, int, error)
	RecvFrameNoWait() ([]byte, int, error)
	RecvMessage() ([][]byte, error)
	RecvMessageNoWait() ([][]byte, error)
}

type lifetime interface {
	Destroy()
}

func destroy(s sender) {
	if sockWithLifetime, ok := s.(lifetime); ok {
		sockWithLifetime.Destroy()
	}
}

type Pub interface {
	SetWriter(p *sender)
	//Send(m *msg.IPubSub)
}

// Sub is implemented by subscribers to receive messages
type Sub interface {
	Receive(m msg.IPubSub)
	Topics() string
}

type SubRouter struct {
	topics map[string]*Sub
	sock   *zmq.Sock
	kill   bool
}

func NewSR(port int) *SubRouter {
	sub, err := zmq.NewSub("tcp://localhost:"+strconv.Itoa(port), "")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return &SubRouter{
		sock:   sub,
		topics: make(map[string]*Sub, 5),
	}
}

// Listen Does not block if there is no message
func (sr *SubRouter) Listen() {
	// TODO: GoRoutines do not work with zmq
	func() {
		binMsg, err := sr.sock.RecvMessageNoWait()
		if err != nil {
			log.Println("Router received: ", err)
			log.Println("Handles messages on topics: ", sr.topics)
			return
		}
		var strMsg = make([]string, len(binMsg))
		for i, bytes := range binMsg {
			strMsg[i] = string(bytes)
		}
		message := strings.Split(strMsg[0], "@")
		if sr.topics[message[0]] != nil {
			sub := *sr.topics[message[0]]
			sub.Receive(&msg.PubSub{
				Topic:    message[0],
				Messages: []string{message[1]},
			})
		}
	}()
}

func (sr *SubRouter) Destroy() {
	sr.kill = true
	sr.sock.Destroy()
}

func (sr *SubRouter) Add(s Sub) {
	if sr.topics[s.Topics()] != nil {
		s = MultiSubs(*sr.topics[s.Topics()], s)
	}
	sr.topics[s.Topics()] = &s
	sr.sock.SetSubscribe(s.Topics())
}

// All handlers are subscribed to Topic of first Sub
func MultiSubs(subs ...Sub) Sub {
	return &MultiSub{
		subs:  subs,
		topic: subs[0].Topics(),
	}
}

type MultiSub struct {
	subs  []Sub
	topic string
}

func (ms *MultiSub) Receive(m msg.IPubSub) {
	for _, sub := range ms.subs {
		sub.Receive(m)
	}
}

func (ms *MultiSub) Topics() string {
	return ms.topic
}

type PrinterSub struct {
	SubTopics string
}

func (ps *PrinterSub) Receive(m msg.IPubSub) {
	fmt.Println(m)
}

func (ps *PrinterSub) Topics() string {
	return ps.SubTopics
}

func (sr *SubRouter) Topics() string {
	keys := make([]string, len(sr.topics))
	i := 0
	for k := range sr.topics {
		keys[i] = k
		i++
	}
	return strings.Join(keys, "/")
}

type PubRouter struct {
	sock sender
}

func NewPubRouter(port int) *PubRouter {
	pub, err := zmq.NewPub("tcp://*:" + strconv.Itoa(port))
	if err != nil {
		log.Println(err)
		return nil
	}
	return WrapSender(pub)
}

func WrapSender(s sender) *PubRouter {
	return &PubRouter{sock: s}
}

func (pr *PubRouter) Send(m msg.IPubSub) {
	binMsg := []byte(fmt.Sprint(m))
	err := pr.sock.SendMessage([][]byte{binMsg})
	if err != nil {
		log.Println("pub router error: ", err)
		return
	}
}

func (pr *PubRouter) Destroy() {
	destroy(pr.sock)
}
